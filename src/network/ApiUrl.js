const BASE_URL = "http://ubux.biz/test/";
const ApiUrl = {
    GET_ALL_STORES: BASE_URL+"get-all-stores",
    SEARCH_STORES: BASE_URL+"search-store",
    GET_STORE_DETAIL: BASE_URL+"get-store",
    GET_STORE_PRODUCTS: BASE_URL+"get-store-products",
    GET_PRODUCT_DETAIL: BASE_URL+"get-product",
}
export default ApiUrl;