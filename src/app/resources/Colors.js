const Colors = {
    white: "#FFFFFF",
    background:"#EBEBEB",
    black: "#000000",
    line: "#CED0CE"
}

export default Colors;