const Images = {
    filter: require("../resources/icons/filter.png"),
    back: require("../resources/icons/back.png"),
    cart: require("../resources/icons/cart.png"),
};
export default Images;
