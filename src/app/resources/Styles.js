import { StyleSheet } from "react-native";
import Colors from "./Colors";
const Styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: Colors.background },
  icon: { width: 30, height: 30 },
  activeTitle: { color: "red" },
  row: { flexDirection: "row" },
  card: {
    backgroundColor: Colors.white,
    borderRadius: 4,
    shadowColor: Colors.black,
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 0.3
    }
  },
  seprator: { height: 1, backgroundColor: Colors.line }
});

export default Styles;
