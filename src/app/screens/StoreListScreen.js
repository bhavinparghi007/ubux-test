import React, { PureComponent } from "react";
import {
  View,
  ActivityIndicator,
  Image,
  TouchableOpacity,
  Text
} from "react-native";
import { connect } from "react-redux";
import * as actions from "../../redux/actions/AppActions";
import StoreListComponent from "../components/StoreListComponent";
import Styles from "../resources/Styles";
import SearchComponent from "../components/SearchComponent";
import Images from "../resources/Images";
import Snackbar from "react-native-snackbar";
import Menu, { MenuItem, MenuDivider } from "react-native-material-menu";

const filterData = [
  {
    label: "Pending",
    value: "pending"
  },
  {
    label: "Verified",
    value: "verified"
  }
];

class StoreListScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      stores: [],
      searchKeyword: ""
    };
  }
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = (status) => {
    this._menu.hide();
    this.filterStores(status);
  };

  showMenu = () => {
    this._menu.show();
  };

  componentDidMount() {
    this.getStores();
  }
  getStores() {
    this.props.getStores();
  }
  filterStores(status) {
    if (status === "select") {
      this.setState({ stores: [] });
      return;
    }
    let filteredData = this.props.stores.filter(function (item) {
      return item.status.includes(status);
    });

    this.setState({ stores: filteredData });

  }
  handleSearch = text => {
    this.setState({ searchKeyword: text }, value => {
      setTimeout(() => {
        if (this.state.searchKeyword === "") {
          this.props.getStores();
        } else {
          if (this.state.searchKeyword.length <= 3) {
          } else {
            this.props.searchStores(this.state.searchKeyword);
          }
        }
      }, 500);
    });
  };
  doShowMessage(message) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_SHORT
    });
  }
  componentWillReceiveProps(nextProps) {

    if (nextProps.error != undefined) {
      this.doShowMessage(nextProps.error);
    }
  }
  render() {
    return (
      <View style={Styles.container}>
        <View style={[Styles.row, { alignItems: "center" }]}>
          <SearchComponent handleSearch={this.handleSearch} />

          <Menu
            ref={this.setMenuRef}
            button={
              <TouchableOpacity onPress={this.showMenu}>
                <Image
                  source={Images.filter}
                  style={[Styles.icon, { margin: 10 }]}
                />
              </TouchableOpacity>
            }
          >
            <MenuItem onPress={() => this.hideMenu("select")}>Select</MenuItem>
            <MenuItem onPress={() => this.hideMenu("pending")}>Pending</MenuItem>
            <MenuItem onPress={() => this.hideMenu("verified")}>Verified</MenuItem>
          </Menu>
        </View>

        <ActivityIndicator
          style={{
            display: this.props.isLoading ? "flex" : "none",
            margin: 10
          }}
        />
        <View style={{ display: this.props.isLoading ? "none" : "flex" }}>
          <StoreListComponent
            navigation={this.props.navigation}
            stores={this.state.stores.length > 0 ? this.state.stores : this.props.stores} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  state: state,
  isLoading: state.app.isLoading,
  stores: state.app.stores,
  error: state.app.error
});

const mapDispatchToProps = dispatch => ({
  getStores: () => dispatch(actions.getStores()),
  searchStores: keyword => dispatch(actions.searchStores(keyword))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoreListScreen);
