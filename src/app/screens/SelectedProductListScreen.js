import React, { PureComponent } from "react";
import {
  View,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import Styles from "../resources/Styles";
import SelectedProductsListComponent from "../components/SelectedProductsListComponent";

class SelectedProductListScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedProducts: props.navigation.state.params.selectedProducts
    };
  }
  render() {
    return (
      <View style={Styles.container}>

        <ActivityIndicator
          style={{
            display: this.props.isLoading ? "flex" : "none",
            margin: 10
          }}
        />
        <View style={{ display: this.props.isLoading ? "none" : "flex" }}>
          <SelectedProductsListComponent
            navigation={this.props.navigation}
            products={this.state.selectedProducts} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  state: state
});

const mapDispatchToProps = dispatch => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedProductListScreen);
