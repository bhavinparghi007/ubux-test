import React, { PureComponent } from "react";
import {
  View,
  ActivityIndicator,
  ScrollView,
  Image,
  TouchableOpacity,
  Text
} from "react-native";
import { connect } from "react-redux";
import * as actions from "../../redux/actions/AppActions";
import Styles from "../resources/Styles";
import Images from "../resources/Images";
import Snackbar from "react-native-snackbar";
import Colors from "../resources/Colors";
import ReviewListComponent from "../components/ReviewListComponent";
import ProductsListComponent from "../components/ProductsListComponent";

class StoreDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      store: props.navigation.state.params.store,
      review: [],
      products: [],
    };
  }
  componentDidMount() {
    this.store = this.props.navigation.state.params.store;
    this.getStoreDetail(this.store.storeId);
  }
  getStoreDetail(storeId) {
    this.props.getStoreDetail(storeId);
    this.props.getStoreProducts(storeId);
  }


  doShowMessage(message) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_SHORT
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps);

    if (nextProps.error != undefined) {
      this.doShowMessage(nextProps.error);
    }
    if (nextProps.products != undefined) {
      this.setState({ products: nextProps.products });
    }
    if (nextProps.store != undefined) {
      if (nextProps.store.review != undefined) {
        this.setState({ review: nextProps.store.review });
      }
    }
  }
  doRedirect(screen) {
    this.props.navigation.navigate(screen, { selectedProducts: this.props.selectedProducts });
  }
  render() {
    return (
      <View style={Styles.container}>


        <ScrollView>
          <View>
            <View style={[Styles.row, { alignItems: "center" }]}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                <Image source={Images.back} style={[Styles.icon, { margin: 10 }]} />
              </TouchableOpacity>
              <View style={{ flex: 1 }} />
              <TouchableOpacity onPress={() => this.doRedirect("SelectedProducts")}>
                <Image source={Images.cart} style={[Styles.icon, { margin: 10 }]} />
              </TouchableOpacity>
            </View>
            <ActivityIndicator
              style={{
                display: this.props.isLoading ? "flex" : "none",
                margin: 10
              }}
            />
            <View style={[Styles.card, { margin: 10 }]}>
              <Text style={{ color: Colors.black, padding: 10 }}>Name : {this.props.store == undefined ? "" : this.props.store.name}</Text>
              <Text style={{ color: Colors.black, padding: 10 }}>Description : {this.props.store == undefined ? "" : this.props.store.description}</Text>
              <Text style={{ color: Colors.black, padding: 10 }}>Email : {this.props.store == undefined ? "" : this.props.store.businessEmail}</Text>
              <Text style={{ color: Colors.black, padding: 10 }}>Suburb : {this.props.store == undefined ? "" : this.props.store.suburb}</Text>
              <Text style={{ color: Colors.black, fontWeight: "600", padding: 10 }}>Contact Details</Text>
              <Text style={{ color: Colors.black, padding: 10 }}>Full Address : {this.props.store == undefined ? "" : this.props.store.fullAddress}</Text>

            </View>
            <View style={[Styles.card, { margin: 10 }]}>
              <Text style={{ color: Colors.black, fontWeight: "600", padding: 10 }}>Products</Text>
              <ProductsListComponent products={this.state.products} navigation={this.props.navigation} />
            </View>

            <View style={[Styles.card, { margin: 10 }]}>
              <Text style={{ color: Colors.black, fontWeight: "600", padding: 10 }}>Reviews</Text>
              <ReviewListComponent reviews={this.state.review} />
            </View>
          </View>

        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  state: state,
  isLoading: state.app.isLoading,
  store: state.app.store,
  products: state.app.products,
  selectedProducts: state.app.selectedProducts,
  error: state.app.error
});

const mapDispatchToProps = dispatch => ({
  getStoreDetail: (storeId) => dispatch(actions.getStoreDetail(storeId)),
  getStoreProducts: (storeId) => dispatch(actions.getStoreProducts(storeId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoreDetailScreen);
