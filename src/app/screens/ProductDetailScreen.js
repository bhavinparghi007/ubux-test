import React, { PureComponent } from "react";
import {
  View,
  ActivityIndicator,
  ScrollView,
  Image,
  Text,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import * as actions from "../../redux/actions/AppActions";
import Styles from "../resources/Styles";
import Snackbar from "react-native-snackbar";
import Colors from "../resources/Colors";
import Images from "../resources/Images";
class ProductDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      product: props.navigation.state.params.product
    };
  }
  componentDidMount() {
    this.product = this.props.navigation.state.params.product;
    this.getProductDetail(this.product._id);
  }
  getProductDetail(productId) {
    this.props.getProductDetail(productId);
  }


  doShowMessage(message) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_SHORT
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps);

    if (nextProps.error != undefined) {
      this.doShowMessage(nextProps.error);
    }
    if (nextProps.product != undefined) {
      this.setState({ product: nextProps.product });
    }


  }
  render() {
    return (
      <View style={Styles.container}>


        <ScrollView>
          <View>
            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
              <Image source={Images.back} style={[Styles.icon, { margin: 10 }]} />
            </TouchableOpacity>
            <ActivityIndicator
              style={{
                display: this.props.isLoading ? "flex" : "none",
                margin: 10
              }}
            />
            <View style={[Styles.card, { margin: 10 }]}>
              <Image source={{ uri: this.state.product.imageUrl }} style={{ width: "100%", height: 150 }} />
              <Text style={{ color: Colors.black, padding: 10 }}>Name : {this.state.product.name}</Text>
              <Text style={{ color: Colors.black, padding: 10 }}>Description : {this.state.product.description}</Text>
              <Text style={{ color: Colors.black, padding: 10 }}>Category : {this.state.product.category}</Text>
              <Text style={{ color: Colors.black, padding: 10 }}>Price : {this.state.product.priceBux}</Text>

            </View>

          </View>

        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  state: state,
  isLoading: state.app.isLoading,
  product: state.app.product,
  error: state.app.error
});

const mapDispatchToProps = dispatch => ({
  getProductDetail: (productId) => dispatch(actions.getProductDetail(productId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDetailScreen);
