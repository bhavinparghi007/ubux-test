import React, { PureComponent } from "react";
import { connect } from "react-redux";
import * as actions from "../../redux/actions/AppActions";
import PropTypes from "prop-types";
import { View, TouchableOpacity, FlatList, Text, Image } from "react-native";
import Colors from "../resources/Colors";
import Styles from "../resources/Styles";
class ProductsListComponent extends PureComponent {
  static propTypes = {
    products: PropTypes.array,
    navigation: PropTypes.object
  };
  doRedirect(item) {
    this.props.navigation.navigate("ProductDetail", { product: item });
  }
  doSelect(item) {
    this.props.addSelectedProduct(item);
    console.log(this.props.selectedProducts);
    
  }
  renderStores(item, index) {
    return (
      <TouchableOpacity onPress={() => this.doSelect(item)} >
        <View style={{ margin: 10 }}>
          <View style={[Styles.card, { alignContent: "center", justifyContent: "center" }]}>
            <Image style={{ width: 180, height: 100 }} resizeMethod="auto" resizeMode="contain" source={{ uri: item.imageUrl }} />
            <Text style={{ color: Colors.black, fontSize: 16 }}>
              {item.name}
            </Text>
            <Text style={{ color: Colors.black }}>
              Category : {item.category}
            </Text>
            <View style={Styles.row}>
              <Text style={{ color: Colors.black, flex: 1 }}>
                Price : {item.priceBux}
              </Text>
              <TouchableOpacity onPress={() => this.doRedirect(item)}>
                <Text style={{ color: Colors.black, flex: 1 }}>
                  More Detail
              </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  renderSeparator = () => {
    return (
      <View style={[Styles.seprator, { marginStart: 10, marginEnd: 10 }]} />
    );
  };
  render() {
    const { products } = this.props;

    return (
      <View>
        <FlatList
          horizontal={true}
          data={products}
          renderItem={({ item, index }) => this.renderStores(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  selectedProducts: state.app.selectedProducts
});

const mapDispatchToProps = dispatch => ({
  addSelectedProduct: (product) => dispatch(actions.addSelectedProduct(product))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsListComponent);
