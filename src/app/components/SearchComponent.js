import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View,  TextInput } from "react-native";
import Colors from "../resources/Colors";
import Styles from "../resources/Styles";
class SearchComponent extends PureComponent {
  static propTypes = {
    handleSearch: PropTypes.func
  };
  
  render() {
    const { handleSearch} = this.props;

    return (
      <View style={{flex:1}}>
       <TextInput
          placeholder="Search.."
          onChangeText={handleSearch}
          style={[
            Styles.card,
            { margin: 10, padding: 10, color: Colors.black }
          ]}
        />
      </View>
    );
  }
}

export default SearchComponent;
