import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, FlatList, Text } from "react-native";
import Colors from "../resources/Colors";
import Styles from "../resources/Styles";
class StoreListComponent extends PureComponent {
  static propTypes = {
    stores: PropTypes.array,
    navigation:PropTypes.object
  };
  doRedirect(item) {
    this.props.navigation.navigate("StoreDetail", { store: item });
  }
  renderStores(item, index) {
    return (
      <TouchableOpacity onPress={()=>this.doRedirect(item)}>
        <View style={{ margin: 10 }}>
          <View style={[Styles.row, Styles.card]}>
            <Text style={{ color: Colors.black, flex: 1, padding: 10 }}>
              {item.tradingName === "" ? "NA" : item.tradingName}
            </Text>
            <Text style={{ color: Colors.black, padding: 10 }}>
              {item.status}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  renderSeparator = () => {
    return (
      <View style={[Styles.seprator, { marginStart: 10, marginEnd: 10 }]} />
    );
  };
  render() {
    const { stores } = this.props;

    return (
      <View>
        <FlatList
          data={stores}
          renderItem={({ item, index }) => this.renderStores(item, index)}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={this.renderSeparator}
        />
      </View>
    );
  }
}

export default StoreListComponent;
