import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, FlatList, Text } from "react-native";
import Colors from "../resources/Colors";
import Styles from "../resources/Styles";
class ReviewListComponent extends PureComponent {
  static propTypes = {
    reviews: PropTypes.array,
    navigation: PropTypes.object
  };
  doRedirect(item) {
    this.props.navigation.navigate("StoreDetail", { store: item });
  }
  renderStores(item, index) {
    return (
      <TouchableOpacity >
        <View style={{ margin: 10 }}>
          <View style={[Styles.row, Styles.card]}>
            <Text style={{ color: Colors.black, flex: 1, padding: 10 }}>
              {item.comment === "" ? "NA" : item.comment}
            </Text>
            <Text style={{ color: Colors.black, padding: 10 }}>
              Rate {item.star}/5
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  renderSeparator = () => {
    return (
      <View style={[Styles.seprator, { marginStart: 10, marginEnd: 10 }]} />
    );
  };
  render() {
    const { reviews } = this.props;

    return (
      <View>
        <FlatList
          data={reviews}
          renderItem={({ item, index }) => this.renderStores(item, index)}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={this.renderSeparator}
        />
      </View>
    );
  }
}

export default ReviewListComponent;
