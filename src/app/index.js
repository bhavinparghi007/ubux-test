import { createStackNavigator, createAppContainer } from "react-navigation";
import StoreListScreen from "./screens/StoreListScreen";
import StoreDetailScreen from "./screens/StoreDetailScreen";
import ProductDetailScreen from "./screens/ProductDetailScreen";
import SelectedProductListScreen from "./screens/SelectedProductListScreen";

const MainNavigator = createStackNavigator(
  {
    Home: {
      screen: StoreListScreen,
      navigationOptions: {
        header: null
      }
    },
    StoreDetail: {
      screen: StoreDetailScreen,
      navigationOptions: {
        header: null
      }
    },
    ProductDetail: {
      screen: ProductDetailScreen,
      navigationOptions: {
        header: null
      }
    },
    SelectedProducts: {
      screen: SelectedProductListScreen,
      navigationOptions: {
        header: null
      }
    },
  },
  {
    initialRouteName: "Home"
  }
);

const MainApp = createAppContainer(MainNavigator);
export default MainApp;
