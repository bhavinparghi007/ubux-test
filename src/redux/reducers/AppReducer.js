import ActionTypes from "../actions/ActionTypes";

const INITIAL_STATE = {
  isLoading: false,
  stores: [],
  products: [],
  selectedProducts: [],
  store: undefined,
  error: undefined,
}

export default function app(state = INITIAL_STATE, action) {
  console.log(action);

  switch (action.type) {
    case ActionTypes.ACTION_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      }

    case ActionTypes.ACTION_STORES:
      return {
        ...state,
        stores: action.data
      }
    case ActionTypes.ACTION_PRODUCTS:
      return {
        ...state,
        products: action.data
      }
    case ActionTypes.ACTION_STORE:
      return {
        ...state,
        store: action.data
      }
    case ActionTypes.ACTION_PRODUCT:
      return {
        ...state,
        product: action.data
      }
    case ActionTypes.ACTION_GET_SELECTED_PRODUCTS:
      return {
        ...state,
        selectedProducts: action.data
      }
    case ActionTypes.ACTION_SELECTED_PRODUCTS:
      return {
        ...state,
        selectedProducts: [...state.selectedProducts, action.data]
      }
    case ActionTypes.ACTION_SELECTED_PRODUCTS_REMOVE:
      let itemToRemove = state.selectedProducts.find(item => action.data._id === item._id)
      let new_items = state.selectedProducts.filter(item => action.data._id !== item._id)
      return {
        ...state,
        selectedProducts: new_items
      }
    case ActionTypes.ACTION_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error
      }
    default:
      return state
  }
}
