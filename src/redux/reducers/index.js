import {combineReducers} from 'redux'
import App from './AppReducer'

const rootReducer = combineReducers({
  app:App
})

export default rootReducer;
