import ActionTypes from "./ActionTypes";
import ApiUrl from "../../network/ApiUrl";

export function isLoading(bool) {
  return {
    type: ActionTypes.ACTION_LOADING,
    isLoading: bool
  };
}

export function setStores(data) {
  return {
    type: ActionTypes.ACTION_STORES,
    data
  };
}
export function setStore(data) {
  return {
    type: ActionTypes.ACTION_STORE,
    data
  };
}
export function setProducts(data) {
  return {
    type: ActionTypes.ACTION_PRODUCTS,
    data
  };
}
export function setProduct(data) {
  return {
    type: ActionTypes.ACTION_PRODUCT,
    data
  };
}
export function setSelectedProduct(data) {
  return {
    type: ActionTypes.ACTION_SELECTED_PRODUCTS,
    data
  };
}
export function deleteSelectedProduct(data) {
  return {
    type: ActionTypes.ACTION_SELECTED_PRODUCTS_REMOVE,
    data
  };
}
export function setSelectedProducts(data) {
  return {
    type: ActionTypes.ACTION_GET_SELECTED_PRODUCTS,
    data
  };
}

export function showError(error) {
  return {
    type: ActionTypes.ACTION_ERROR,
    error
  };
}

export function getStores() {
  return dispatch => {
    dispatch(isLoading(true));
    return fetch(ApiUrl.GET_ALL_STORES, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        const success = responseJSON.success;
        const message = responseJSON.message;
        if (success) {
          const stores = responseJSON.stores;
          dispatch(setStores(stores));
        } else {
          dispatch(showError(message));
        }
        dispatch(isLoading(false));
      })
      .catch(error => {
        console.log("error", error);
        dispatch(isLoading(false));
        dispatch(showError(error));
      });
  };
}
export function searchStores(keyword) {
  console.log(keyword);

  return dispatch => {
    dispatch(isLoading(true));
    dispatch(setStores([]));
    return fetch(ApiUrl.SEARCH_STORES, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ keyword: keyword })
    })
      .then(response => response.json())
      .then(responseJSON => {
        console.log(responseJSON);

        const success = responseJSON.success;
        const message = responseJSON.message;
        if (success) {
          const stores = responseJSON.stores;
          dispatch(setStores(stores));
        } else {
          dispatch(showError(message));
        }
        dispatch(isLoading(false));
      })
      .catch(error => {
        console.log("error", error);
        dispatch(isLoading(false));
        dispatch(showError(error));
      });
  };
}
export function getStoreDetail(storeId) {
  return dispatch => {
    dispatch(isLoading(true));

    return fetch(ApiUrl.GET_STORE_DETAIL + "?storeId=" + storeId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        console.log(responseJSON);

        const success = responseJSON.success;
        const message = responseJSON.message;
        if (success) {
          const store = responseJSON.store;
          dispatch(setStore(store));
        } else {
          dispatch(setStore(undefined));
          dispatch(showError(message));
        }
        dispatch(isLoading(false));
      })
      .catch(error => {
        console.log("error", error);

        dispatch(isLoading(false));
        dispatch(showError(error));
      });
  };
}
export function getStoreProducts(storeId) {
  return dispatch => {
    dispatch(setProducts([]))
    dispatch(isLoading(true));
    return fetch(ApiUrl.GET_STORE_PRODUCTS + "?storeId=" + storeId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        console.log(responseJSON);

        const success = responseJSON.success;
        const message = responseJSON.message;
        if (success) {
          const products = responseJSON.products;
          dispatch(setProducts(products));
        } else {
          dispatch(setProducts([]));
          dispatch(showError(message));
        }
        dispatch(isLoading(false));
      })
      .catch(error => {
        console.log("error", error);
        dispatch(setProducts([]));
        dispatch(isLoading(false));
        dispatch(showError(error));
      });
  };
}
export function getProductDetail(productId) {
  return dispatch => {
    dispatch(isLoading(true));
    return fetch(ApiUrl.GET_PRODUCT_DETAIL + "?productId=" + productId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        console.log(responseJSON);

        const success = responseJSON.success;
        const message = responseJSON.message;
        if (success) {
          const product = responseJSON.product;
          dispatch(setProduct(product));
        } else {
          dispatch(setProduct(undefined));
          dispatch(showError(message));
        }
        dispatch(isLoading(false));
      })
      .catch(error => {
        console.log("error", error);
        dispatch(setProduct(undefined));
        dispatch(isLoading(false));
        dispatch(showError(error));
      });
  };
}

export function addSelectedProduct(product) {
  return dispatch => {
    dispatch(setSelectedProduct(product));
  }
}
export function removeSelectedProduct(product) {
  return dispatch => {
    dispatch(deleteSelectedProduct(product));
  }
}
export function getSelectedProducts(data) {
  return dispatch => {
    dispatch(setSelectedProducts(data));
  }
}

