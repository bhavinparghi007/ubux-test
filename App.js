import React, { PureComponent } from "react";
import { Provider } from "react-redux";
import store from "./src/redux/store";
import MainApp from "./src/app/index";

export default class App extends PureComponent {
  render() {
    return (
      <Provider store={store}>
        <MainApp />
      </Provider>
    );
  }
}
